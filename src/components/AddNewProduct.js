import React from 'react';
import ProductForm from './ProductForm';

export class AddNewProduct extends React.Component {
    render() {
        return (
            <ProductForm headTitle="ADD NEW PRODUCT" />
        );
    }
}

export default AddNewProduct;