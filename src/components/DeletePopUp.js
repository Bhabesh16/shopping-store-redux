import React from 'react';
import { connect } from 'react-redux';

class DeletePopUp extends React.Component {

    handleClickDelete = () => {
        this.props.deleteProduct(this.props.productId);
    }

    render() {
        return (
            <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Delete Product</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div className="modal-body">
                            <h4>Are you sure?</h4>
                            <p>Do you really want to delete this product?</p>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="button" className="btn btn-primary" data-bs-dismiss="modal" onClick={this.handleClickDelete}>Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteProduct: (productId) => dispatch(
            {
                type: 'DELETE_PRODUCT',
                payload: productId
            }
        )
    }
}

export default connect(null, mapDispatchToProps)(DeletePopUp);