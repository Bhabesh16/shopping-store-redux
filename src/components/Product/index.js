import React from 'react';
import { Link } from 'react-router-dom';

import './Product.css';
import ProductDescription from '../layout/ProductDescription';
import DeletePopUp from '../DeletePopUp';
import { connect } from 'react-redux';

class Product extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showMenu: false,
            addCartText: false,
            addCartAddedText: false
        };
    }

    handleClickMenu = () => {
        this.setState((currentState) => {
            return {
                showMenu: !currentState.showMenu,
            }
        });
    }

    handleAddToCart = () => {
        const findProduct = this.props.cartProducts
            .find((product) => {
                return product.id === this.props.product.id;
            });
        if (findProduct === undefined) {
            this.setState({
                addCartText: !this.state.addCartText
            });

            const cartProduct = {
                ...this.props.product,
                quantity: 1
            }

            this.props.addProductToCart(cartProduct);

            setTimeout(() => {
                this.setState({
                    addCartText: !this.state.addCartText
                });
            }, 2000);
        } else {
            this.setState({
                addCartAddedText: !this.state.addCartAddedText
            });

            setTimeout(() => {
                this.setState({
                    addCartAddedText: !this.state.addCartAddedText
                });
            }, 2000);
        }
    }

    render() {
        return (
            <div className='product'>
                <img
                    className='productImage'
                    src={this.props.product.image}
                    alt=''
                />
                <span className='options'
                    onClick={this.handleClickMenu}>...</span>

                {this.state.showMenu &&
                    <div className='popup-menu' >
                        <span id="edit-product"><Link to={`update-product/${this.props.product.id}`} style={{ color: '#000', textDecoration: 'none' }}>edit product</Link></span>
                        <span
                            id="delete-product"
                            data-bs-toggle="modal"
                            data-bs-target="#exampleModal"
                            onClick={this.handleClickDelete}
                        >
                            delete product
                        </span>
                        <DeletePopUp productId={this.props.product.id} />
                    </div>
                }

                <ProductDescription title={this.props.product.title} rating={this.props.product.rating} />

                <div className='priceContainer'>
                    <h4>${this.props.product.price}</h4>
                    {this.state.addCartText && <div className='cart-add-text'>Item added to the cart</div>}
                    {!this.state.addCartText && this.state.addCartAddedText && <div className='cart-add-text'>Item already added</div>}
                    <i className='fal fa-shopping-cart cart' onClick={this.handleAddToCart}></i>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    const cartProducts = state.cart.list;
    return {
        cartProducts
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        addProductToCart: (product) => dispatch({
            type: "ADD_PRODUCT_TO_CART",
            payload: product
        })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Product);