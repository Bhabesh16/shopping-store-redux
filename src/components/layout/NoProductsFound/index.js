import React from 'react';

import './NoProductsFound.css';
import noProduct from '../../../assets/images/no-product.png';

class NoProductsFound extends React.Component {
    render() {

        return (
            <div className="no-products-container">
                <img
                    className='no-products-img'
                    src={noProduct}
                    alt=''
                />
                <div className="no-products-text-container">
                    <span className="no-products-heading">{this.props.head}</span>
                    <div>
                        <div className="no-products-message">{this.props.message}</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default NoProductsFound;