import React from 'react';

import './Loader.css';

class Loader extends React.Component {
    render() {
        return (
            <div className="loaderLogo">
                <div className="lds-facebook">
                    <div></div>
                    <div></div>
                    <div></div>
                </div>
            </div>
        );
    }
}

export default Loader;