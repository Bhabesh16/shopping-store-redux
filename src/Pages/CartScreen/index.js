import React from 'react';
import { connect } from 'react-redux';

import './CartScreen.css';
import CartProduct from '../../components/CartProduct';
import Loader from '../../components/layout/Loader';
import NoProductsFound from '../../components/layout/NoProductsFound';

class CartScreen extends React.Component {

  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded"
    };

    this.state = {
      status: this.API_STATES.LOADING,
      totalAmount: 0
    };
  }

  getTotalAmount = () => {
    const allTotalAmount = this.props.cartProducts
      .reduce((allSum, product) => {
        allSum += (product.quantity * product.price);
        return allSum;
      }, 0);

    this.setState({
      totalAmount: allTotalAmount
    })
  }

  componentDidMount() {
    this.setState({
      status: this.API_STATES.LOADED
    });

    this.getTotalAmount();
  }

  render() {

    return (
      <div id="cart-container">
        {this.state.status === this.API_STATES.LOADING && <Loader />}
        {this.state.status === this.API_STATES.LOADED && this.props.cartProducts.length === 0 && <NoProductsFound head="Add Products to Cart" message="No products found, please add some products to the cart." />}
        {this.state.status === this.API_STATES.LOADED && this.props.cartProducts.length > 0 &&
          <>
            <h3>Shopping Cart<span>{` (${this.props.cartProducts.length} items)`}</span></h3>
            <div className="cart-details-container">
              <span className='cart-item-details'>Item Details</span>
              <span className='cart-item-price' style={{ color: 'black' }}>Price</span>
              <span className='cart-item-quantity'>Quantity</span>
              <span className='cart-item-subtotal' style={{ color: 'black' }}>Subtotal</span>
            </div>
            {
              this.props.cartProducts.map((cartProduct) => {
                return <CartProduct
                  key={cartProduct.id}
                  cartProduct={cartProduct}
                  getTotalAmount={this.getTotalAmount}
                />;
              })
            }
            <div className='checkout-container'>
              <span className='checkout-text'>Sub Total: <span>${this.state.totalAmount.toFixed(2)}</span></span>
              <button>PROCEED TO PAY</button>
            </div>
          </>
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const cartProducts = state.cart.list;
  return {
    cartProducts
  };
}

export default connect(mapStateToProps)(CartScreen);